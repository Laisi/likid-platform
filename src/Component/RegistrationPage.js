import React, { Fragment } from "react";
import {
  Container,
  Col,
  Row,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Navbar,
  NavbarBrand,
  Modal,
  ModalHeader,
  ModalBody,
} from "reactstrap";

// Experimental.
// function Test() {
//   const [modal, setModal] = useState(false);
//   const toggle = () => useState(!modal)
//   return (
//     <div>
//       <Modal isOpen={modal} toggle={toggle()}>
//         <ModalHeader toggle={toggle()}>Client Details</ModalHeader>
//         <ModalBody>
//           {/* Add response here. */}
//         </ModalBody>
//       </Modal>
//     </div>
//   )
// }

export default function Registration(props) {
  function submit(e) {
    e.preventDefault();
    let form = e.currentTarget;
    let data = new FormData(form);
      let body = {};
      for (let pair of data.entries()) body[pair[0]] = pair[1];
    // const { history } = props;
    const headers = {
      "Content-Type": "application/json"
    };
    body = JSON.stringify(body);
    console.log(body);
    fetch(
      "https://brokers-api.huntrecht-live.eu-gb.containers.appdomain.cloud/v1/account",
      {
        method: "POST",
        body: body,
        headers: headers
      }
    )
      .then(res => res.json() && props.history.push('/'))
      .catch(err => console.log(err.message));   
  }

  return (
    <Fragment>
      <Navbar color="light" light expand="md">
        <NavbarBrand> Join Likid </NavbarBrand>
      </Navbar>
      <Container className="App" fluid>
        <br />
        <Form onSubmit={submit}>
          <FormGroup>
            <Label for="likidEmail">Email</Label>
            <Input
              type="email"
              name="email"
              id="likidEmail"
              placeholder="john.doe@doe.com"
            />
          </FormGroup>
          <FormGroup>
            <Label for="accountName">Account Name</Label>
            <Input
              type="text"
              name="accountName"
              id="likidAccounttName"
              placeholder="John Doe"
            />
          </FormGroup>
          <FormGroup>
            <Label for="firstName">First Name</Label>
            <Input
              type="text"
              name="firstname"
              id="likidFirstName"
              placeholder="John"
            />
          </FormGroup>
          <FormGroup>
            <Label for="firstName">Last Name</Label>
            <Input
              type="text"
              name="lastname"
              id="likidLastName"
              placeholder="Doe"
            />
          </FormGroup>
          <Row form>
            <Col md={6}>
              <FormGroup>
                <Label for="likidPassword">Password</Label>
                <Input
                  type="password"
                  name="password"
                  id="likidPassword"
                  placeholder=""
                />
              </FormGroup>
            </Col>
          </Row>
          <FormGroup>
            <Label for="likidSelect">Country</Label>
            <Input type="select" name="country" id="likidCountry">
              <option>Nigeria</option>
              <option>Ghana</option>
            </Input>
          </FormGroup>
          <Row form>
            <Col md={6}>
              <FormGroup>
                <Label for="likidCity">City</Label>
                <Input type="text" name="city" id="likidCity" />
              </FormGroup>
            </Col>
          </Row>
          <FormGroup>
            <Label for="likidAddress">Address</Label>
            <Input
              type="text"
              name="address"
              id="likidAddress"
              placeholder="1234 Main St"
            />
          </FormGroup>
          <Button>Sign Up</Button>
        </Form>
        <br />
      </Container>
    </Fragment>
  );
}
