import React, { useState, useEffect } from "react";
import { Route } from "react-router-dom";
import {
  Row,
  Col,
  Form,
  Label,
  Input,
  Button,
  FormGroup,
  Container,
  Card,
  CardHeader,
  CardBody,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";
import UserProfile from "./UserProfile";



export default function LoginPage(props) {
// TODO: Work redirection!
  const [userCred, setUserCred] = useState({});
  const [redirect, setRedirect] = useState(false);

  const { history } = props;
  const abortController = new AbortController();
  const signal = abortController.signal;
  function onSubmit(e) {
    e.preventDefault();
    let form = e.currentTarget;
    let data = new FormData(form);
      let body = {};
      for (let pair of data.entries()) body[pair[0]] = pair[1];
    const headers = {
      "Content-Type": "application/json"
    };
    body = JSON.stringify(body);

    fetch(
      "https://brokers-api.huntrecht-live.eu-gb.containers.appdomain.cloud/v1/authenticate",
      {
        method: "POST",
        body: body,
        headers: headers,
	signal: signal
      }
    )
      .then(res => res.json())
      .then(res => setUserCred(res))
      .then(setRedirect(userCred.success))
      .then(history.push('/profile'))
      .catch(err => console.log(err.message));
  }
  // Experimental.
    useEffect(() => {
      return function cleanup() {
	abortController.abort();
      }
    }, []);

    return (
    <Container className="App" fluid>
      <br />
      <Card>
        <CardHeader align="center"> Login </CardHeader>
        <CardBody>
          <Row form>
            <Col sm="6" md={{ size: 6, offset: 3 }}>
              <Form onSubmit={onSubmit}>
                <Col>
                  <FormGroup>
                    <Label for="emailaddr">Email:</Label>
                    <Input
                      type="Email"
                      name="email"
                      id="mail"
                      placeholder="Email Address"
                      required
                    />
                  </FormGroup>
                </Col>
                <Col>
                  <FormGroup>
                    <Label for="password">Password:</Label>
                    <Input
                      type="password"
                      name="password"
                      id="password"
                      placeholder="Password"
                      required
                    />
                  </FormGroup>
                </Col>
                <Button color="primary">Submit </Button>
              </Form>
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Nav>
        <NavItem>
          <NavLink href="/join">Don't have an account ? Join Likid </NavLink>
        </NavItem>
      </Nav>
    </Container>
  );
}
