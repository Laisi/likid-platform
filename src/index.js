import React from "react";
import ReactDOM from "react-dom";
import { Route, BrowserRouter as Router } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import LoginPage from "./Component/LoginPage";
import RegistrationPage from "./Component/RegistrationPage";
import UserProfile from "./Component/UserProfile";

import './styles.css';

const rootElement = document.getElementById("root");
ReactDOM.render(
  <Router>
    <Route exact path="/" component={LoginPage} />
    <Route path="/join" component={RegistrationPage} />
    <Route path="/profile" component={UserProfile} />
  </Router>,
  rootElement
);
